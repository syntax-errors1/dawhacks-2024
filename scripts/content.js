"use strict";
const QDNstr=".carte-QDN:not(#MsgAucunQD9):not(.skeleton)";
let QDN=document.querySelectorAll(QDNstr);
changeAccBalance(QDN);
changeMios(QDN);
const assignmentsTable=document.querySelector("#tabListeTravEtu");
const gradeTable = document.querySelector(".table-notes");
const leaCardHeaders=document.querySelectorAll(".card-panel-header");

chrome.storage.local.get(["schoolname"], (res) => {
  const institutionName=document.getElementById("headerNavbarProfileInstitution");
  changeText(institutionName, res.schoolname);
});

chrome.storage.local.get(['customimage'], (res) => {
  const headerimage = document.getElementById('headerImage');
  headerimage.style.background = `url(${res.customimage}) no-repeat top left`;
  headerimage.style["background-size"] = `cover`;
  headerimage.style["background-position"] = 'center';
});

chrome.storage.local.get(['submit'], (res) => {
  const submits = res.submit.split('\n');
  submits.forEach(x => {
    const [assignment, due] = x.split('=')
    changeDueDate(assignment, due);
  })
});

chrome.storage.local.get(['avg'], (res) => {
  const avgs = res.avg.split('\n');
  avgs.forEach(x => {
    const [assignment, avg] = x.split('=');
    changeAverage(assignment, avg);
  })
});

chrome.storage.local.get(['grades'], (res) => {
  const grades = res.grades.split('\n');
  grades.forEach(x => {
    const [assignment, grade] = x.split('=');
    changeGrade(assignment, grade);
  });
});

chrome.storage.local.get(['comment'], (res) => {
  const comments = res.comment.split('\n');
  comments.forEach(x => {
    const [assignment, comment] = x.split('=');
    changeGradeComment(assignment, comment);
  })
})


changeText(document.querySelector("#headerOmnivoxLogo"), " Omnitrix ");

// leaCardHeaders.forEach((header) => {
//     if(header.innerText){
//       changeLeaCardHeaderContent(event.target, "test")}
//     }
// });

function changeText(element, text){
    element.innerText=text;
}

function changeAccBalance(notifs){
    // console.log(notifs);
    if (notifs.length === 0) {
      setTimeout(() => {
        QDN=document.querySelectorAll(QDNstr);
        changeAccBalance(QDN);
      }, 100);
    }
    for (let i = 0; i < notifs.length; i++) {
        const element = notifs[i];
        if(element.innerText.includes("Account balance of")){
    
            chrome.storage.local.get(['balance'], (res) => {
              if (res.balance === "default") {
                return;
              }
              changeText(element.querySelector(".carte-portail-titre"), "Account balance of "+res.balance+" $");
            })
            
        }
    }
}

function changeMios(notifs){
  // console.log(notifs);
  if (notifs.length === 0) {
    setTimeout(() => {
      QDN=document.querySelectorAll(QDNstr);
      changeMios(QDN);
    }, 100);
  }
  for (let i = 0; i < notifs.length; i++) {
      const element = notifs[i];
      if(element.innerText.includes("new Mio")){
  
          chrome.storage.local.get(['mios'], (res) => {
            if (res.mios === "default") {
              return;
            }
            changeText(element.querySelector(".carte-portail-titre"), res.mios +" new Mio");
          })
          
      }
  }
}

function changeDueDate(assignmentName, newDate){
    //const re= new RegExp(assignmentName);
    const dateIndex=2;
    const tds = assignmentsTable.querySelectorAll('td');
    for(let i = 0; i < tds.length; i++){
        if(tds[i].textContent.includes(assignmentName)){
            tds[i+1].textContent = `\n                                    \n                                    \n                                        ${newDate} \n                                    \n                                    \n                                    \n                                        via Léa\n                                    \n                                `
        }
    }
}
function changeGrade(gradeName, newGrade){
    console.log(newGrade);
    if (validateGrade(newGrade) == false){
        console.log("input a grade that is like: number/number");
    }
    else {
        let percentage = getPercentageGrade(newGrade);
        console.log(percentage);
        const tds = gradeTable.querySelectorAll('td');
        for (let i =0; i < tds.length; i++){
            if(tds[i].textContent.includes(gradeName)){
                tds[i+1].innerHTML = `<font size="2" face="Arial, Helvetica" color="#000000"><b>${newGrade}</b></font><br><font size="1" face="Arial, Helvetica" color="#000000">${percentage}%</font>`;
                let weight = tds[i+3].childNodes[0].childNodes[0].innerText.split('%')[0];
                let weightedGrade = (percentage * weight) / 100;
                tds[i+3].childNodes[2].innerText = `(${weightedGrade}/${weight})`
            }
        }
    }
}

function changeLogo() {
    const logoHeader = document.querySelector('#headerOmnivoxLogo');
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute('width', 34);
    svg.setAttribute('height', 34);
    svg.setAttributeNS(null, 'viewBox', '0 0 33 39');
    
    var path1 = document.createElementNS('http://www.w3.org/2000/svg',"path");  
    path1.setAttributeNS(null, 'fill', '#FC8D33');
    path1.setAttributeNS(null, 'fill-rule', 'evenodd');
    path1.setAttributeNS(null, 'd', 'M16.4 21.697c-2.946 0-5.334-2.445-5.334-5.46 0-3.017 2.388-5.461 5.333-5.461 2.946 0 5.334 2.444 5.334 5.46s-2.388 5.46-5.334 5.46m9.943-2.078a6.054 6.054 0 0 0 6.057-6.05 6.054 6.054 0 0 0-6.057-6.05 6.052 6.052 0 0 0-4.944 2.555 7.698 7.698 0 0 0-.222-.175 6.021 6.021 0 0 0 1.383-3.848A6.053 6.053 0 0 0 16.502 0a6.054 6.054 0 0 0-6.057 6.05c0 1.462.519 2.803 1.383 3.848-.12.091-.237.185-.352.283a6.053 6.053 0 0 0-5.02-2.663A6.054 6.054 0 0 0 .4 13.568a6.054 6.054 0 0 0 6.057 6.05c1.03 0 1.999-.257 2.848-.709.08.201.168.398.264.59a6.053 6.053 0 0 0-5.283 6.002 6.053 6.053 0 0 0 6.057 6.05 6.053 6.053 0 0 0 5.81-7.767 7.848 7.848 0 0 0 .93-.014 6.053 6.053 0 0 0 5.805 7.78 6.053 6.053 0 0 0 6.058-6.05 6.053 6.053 0 0 0-5.498-6.023c.078-.158.15-.318.217-.48a6.039 6.039 0 0 0 2.678.621');
    svg.appendChild(path1);

    var g1 = document.createElementNS('http://www.w3.org/2000/svg',"g");  
    g1.setAttributeNS(null, 'transform', 'matrix(0.009887, 0, 0, -0.008843, -221.173179, -138.769886)');
    g1.setAttributeNS(null, 'fill', '#000000');
    g1.setAttributeNS(null, 'strokes', 'none');
    g1.setAttributeNS(null, 'style', '');

    var path2 = document.createElementNS('http://www.w3.org/2000/svg',"path");  
    path2.setAttributeNS(null, 'd', 'M2320 4260 c-511 -37 -1027 -308 -1362 -717 -252 -308 -409 -692 -448 -1097 -12 -125 -8 -230 16 -386 64 -414 262 -807 554 -1099 181 -181 334 -291 552 -396 302 -145 580 -203 932 -192 228 6 392 35 596 103 100 34 321 141 412 200 562 362 921 1001 920 1639 0 492 -202 978 -561 1348 -423 437 -976 642 -1611 597z m456 -470 c195 -36 363 -99 527 -199 92 -56 195 -135 203 -156 5 -11 -75 -144 -315 -523 -70 -111 -291 -489 -291 -498 0 -2 -11 -22 -25 -44 -14 -22 -25 -45 -25 -51 0 -10 93 -177 222 -400 31 -52 100 -165 154 -250 173 -274 277 -444 281 -459 5 -18 -41 -57 -160 -137 -532 -358 -1290 -328 -1795 71 -34 27 -62 53 -62 58 0 12 37 75 134 228 145 231 279 444 296 475 9 16 65 114 123 217 59 102 107 191 107 197 0 16 -257 460 -366 633 -265 418 -297 472 -287 487 21 36 195 155 308 211 129 64 259 110 383 134 112 22 121 23 307 25 133 1 195 -3 281 -19z');
    g1.appendChild(path2);
    svg.appendChild(g1);

    var g2 = document.createElementNS('http://www.w3.org/2000/svg',"g"); 
    g2.setAttributeNS(null, 'transform', 'matrix(0.002755, 0, 0, -0.00293, 9.508238, 23.009045)');
    g2.setAttributeNS(null, 'fill', '#000000');
    g2.setAttributeNS(null, 'strokes', 'none');
    g2.setAttributeNS(null, 'style', '');

    var path3 = document.createElementNS('http://www.w3.org/2000/svg',"path"); 
    path3.setAttributeNS(null, 'd', 'M2320 4260 c-511 -37 -1027 -308 -1362 -717 -252 -308 -409 -692 -448 -1097 -12 -125 -8 -230 16 -386 64 -414 262 -807 554 -1099 181 -181 334 -291 552 -396 302 -145 580 -203 932 -192 228 6 392 35 596 103 100 34 321 141 412 200 562 362 921 1001 920 1639 0 492 -202 978 -561 1348 -423 437 -976 642 -1611 597z m456 -470 c195 -36 363 -99 527 -199 92 -56 195 -135 203 -156 5 -11 -75 -144 -315 -523 -70 -111 -291 -489 -291 -498 0 -2 -11 -22 -25 -44 -14 -22 -25 -45 -25 -51 0 -10 93 -177 222 -400 31 -52 100 -165 154 -250 173 -274 277 -444 281 -459 5 -18 -41 -57 -160 -137 -532 -358 -1290 -328 -1795 71 -34 27 -62 53 -62 58 0 12 37 75 134 228 145 231 279 444 296 475 9 16 65 114 123 217 59 102 107 191 107 197 0 16 -257 460 -366 633 -265 418 -297 472 -287 487 21 36 195 155 308 211 129 64 259 110 383 134 112 22 121 23 307 25 133 1 195 -3 281 -19z');
    g2.appendChild(path3);
    svg.appendChild(g2);

    logoHeader.prepend(svg);
}

changeLogo();

// changeDueDate('CV submission', '28-jun-2028');
function changeAverage(gradeName, newGrade){
    if (validateGrade(newGrade) == false){
        console.log("input a grade that is like: number/number");
    }
    else {
        const tds = gradeTable.querySelectorAll('td');
        for (let i =0; i < tds.length; i++){
            if(tds[i].textContent.includes(gradeName)){
                tds[i+2].childNodes[0].innerHTML = `<b>${newGrade}</b>`;
            }
        }
    }

}

function validateGrade(grade){
    if (grade.match('\-?[0-9]+.?[0-9]*/[0-9]+.?[0-9]*')){
        return true;
    }
    else {
        return false;
    }
}

function getPercentageGrade(grade){
    let numbers = grade.split('/');
    let percentage = (numbers[0]/numbers[1]) * 100;
    return percentage;
}


function changeLeaCardHeaderContent(card, text){
    let content=card.querySelector(".card-panel-desc");
    changeText(content, text);
}

function changeGradeComment(gradeName, newComment){
    const tds = gradeTable.querySelectorAll('td');
        for (let i =0; i < tds.length; i++){
            if(tds[i].textContent.includes(gradeName)){
                tds[i+5].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[2].innerText = `${newComment}`;
            }
        }
}
