document.addEventListener("DOMContentLoaded", main);

const defaltValues = {
  schoolname: "Dawson College",
  customimage: "https://preview.redd.it/a-night-in-la-v0-d4msmdlikf1c1.jpeg?auto=webp&s=42a3b0dc3f1433f3d2244c705f594935728618e9",
  balance: "default",
  submit: "",
  avg: "",
  grades: "",
  comment: "",
  mios: "default",
  leaText: ""
}

function fetchValue(item) {

  const configKey = item.id;
  
  chrome.storage.local.get([configKey], (res) => {

    if (!res[configKey]) {

      res[configKey] = defaltValues[configKey]?? "0";

      var set = {};
      set[configKey] = defaltValues[configKey];

      chrome.storage.local.set(set, () => {});

    }

    item.value = res[configKey];

  });

}

function main() {

  const schoolnamein = document.querySelector("#schoolname");
  const balancein = document.querySelector("#balance");
  const mios = document.querySelector("#mios");
  const customimage = document.querySelector("#customimage")
  const submit = document.querySelector("#submit")
  const avg = document.querySelector("#avg")
  const grades = document.querySelector("#grades")
  const comment = document.querySelector("#comment")
  // const leaText = document.querySelector("#leaText");

  fetchValue(submit);
  fetchValue(schoolnamein);
  fetchValue(balancein);
  fetchValue(customimage);
  fetchValue(avg);
  fetchValue(grades);
  // fetchValue(leaText);
  fetchValue(mios)
  fetchValue(comment);

  mios.addEventListener("input", TextSettingChange);
  schoolnamein.addEventListener("input", TextSettingChange);
  balancein.addEventListener("input", TextSettingChange);
  customimage.addEventListener('input', TextSettingChange);
  submit.addEventListener('input', TextSettingChange);
  avg.addEventListener('input', TextSettingChange);
  grades.addEventListener('input', TextSettingChange);
  comment.addEventListener('input', TextSettingChange);
  leaText.addEventListener('input', TextSettingChange);

}

function TextSettingChange(e) {
  
  const settingname = e.target.id;
  const set = {}
  set[settingname] = e.target.value;
  
  chrome.storage.local.set(set, () => {
    
  })


}